package edu.utfpr.ppgcc.ip.transformations.effects;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import edu.utfpr.ppgcc.ip.transformations.MaskIterator;
import edu.utfpr.ppgcc.ip.transformations.MaskIterator.Pixel;
import edu.utfpr.ppgcc.ip.utils.ImageFromMatrix;
import javafx.scene.image.Image;

public class MaskIteratorTest {
	@Test
	public void testSimpleMaskInTopBoder() {
		Image image = sampleImage();
		List<Pixel> list = new MaskIterator(3, 3).generateStream(image.getPixelReader(), 1, 0)
				.collect(Collectors.toList());
		assertEquals(6, list.size());
	}
	@Test
	public void testSimpleMaskInSideBoder() {
		Image image = sampleImage();
		List<Pixel> list = new MaskIterator(3, 3).generateStream(image.getPixelReader(), 0, 1)
				.collect(Collectors.toList());
		assertEquals(6, list.size());
	}
	@Test
	public void testSimpleMaskInBoder() {
		Image image = sampleImage();
		List<Pixel> list = new MaskIterator(3, 3).generateStream(image.getPixelReader(), 0, 0)
				.collect(Collectors.toList());
		assertEquals(4, list.size());
	}

	@Test
	public void testSimpleMaskOutOfBoder() {
		Image image = sampleImage();
		List<Pixel> list = new MaskIterator(3, 3).generateStream(image.getPixelReader(), 1, 1)
				.collect(Collectors.toList());
		assertEquals(9, list.size());
	}

	private Image sampleImage() {
		return ImageFromMatrix.createGray(new int[][] { { 0, 9, 18, 0, 9 }, { 9, 18, 0, 9, 18 }, { 18, 0, 9, 18, 24 },
				{ 0, 9, 18, 0, 9 }, { 18, 0, 9, 18, 0 } }, 24);
	}
}
