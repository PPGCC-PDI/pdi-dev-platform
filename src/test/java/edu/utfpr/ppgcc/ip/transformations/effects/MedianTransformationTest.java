package edu.utfpr.ppgcc.ip.transformations.effects;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.utfpr.ppgcc.ip.utils.ImageFromMatrix;
import javafx.scene.image.Image;

public class MedianTransformationTest {
	MedianTransformation transformation = new MedianTransformation();

	@Test
	public void testSimpleGrayImage() {
		Image image = ImageFromMatrix.createGray(new int[][] { { 0, 9, 18, 0, 9 }, { 9, 18, 0, 9, 18 },
				{ 18, 0, 9, 18, 24 }, { 0, 9, 18, 0, 9 }, { 18, 0, 9, 18, 0 } }, 24);
		Image apply = transformation.apply(image);
		assertNotEquals(image, apply);
		assertEquals(image.getWidth(), apply.getWidth(), 1);
		assertEquals(image.getHeight(), apply.getHeight(), 0.1);
	}
}
