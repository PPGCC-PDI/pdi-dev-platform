package edu.utfpr.ppgcc.ip.utils;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ImageFromMatrix {

	public static Image createGray(int[][] image, int scale) {
		WritableImage result = new WritableImage(image.length, image[0].length);
		PixelWriter writer = result.getPixelWriter();
		for (int i = 0; i < image.length; i++) {
			for (int j = 0; j < image[i].length; j++) {
				int v = image[i][j];
				double gray = v / scale;
				writer.setColor(i, j, new Color(gray, gray, gray, 1.0));
			}
		}
		return result;
	}

}
