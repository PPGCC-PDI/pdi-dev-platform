package edu.utfpr.ppgcc.ip;

import static javafx.collections.FXCollections.observableArrayList;
import static javafx.collections.FXCollections.synchronizedObservableList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import edu.utfpr.ppgcc.ip.formats.FormatCatalog;
import edu.utfpr.ppgcc.ip.transformations.Transformation;
import edu.utfpr.ppgcc.ip.transformations.multi.MultiTransformation;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

public class ImageTab {
	public static final String FXML = "/" + ImageTab.class.getCanonicalName().replace(".", "/") + ".fxml";

	private static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();

	private static final String EMPTY_FILE = "Nenhum...";

	private final ObservableList<ImageData> images = synchronizedObservableList(observableArrayList());

	@FXML
	private Tab tab;

	@FXML
	private BorderPane root;

	@FXML
	private ScrollPane content;

	@FXML
	private ListView<ImageData> list;

	@FXML
	public void initialize() {
		this.tab.setText(EMPTY_FILE);
		this.images.addListener(this::onImagesChange);
		this.list.setItems(images);
		this.list.getItems().addListener((Change<? extends ImageData> e) -> {
			if (this.list.getItems().size() > 1) {
				this.root.setLeft(list);
			} else {
				this.root.setLeft(null);
			}
		});
		getSelectedImages().addListener(this::onSelectedImagesChange);
	}

	public boolean isControllerOf(Tab tab) {
		return this.tab.equals(tab);
	}

	public void add(List<File> files) {
		files.stream().forEachOrdered(this::add);

	}

	public void add(File file) {
		EXECUTOR.submit(() -> {
			try {
				ImageData data = new ImageData(file);
				Platform.runLater(() -> {
					this.images.add(data);
				});

			} catch (IOException e) {
				this.showError(e);
			}
		});
	}

	public void onSelectedImagesChange(ListChangeListener.Change<? extends ImageData> change) {
		this.drawImages();
	}

	public void onImagesChange(ListChangeListener.Change<? extends ImageData> change) {
		tab.setText(change.getList().stream().map(ImageData::getName).collect(Collectors.joining(", ")));
		Platform.runLater(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.list.getSelectionModel().selectAll();
		});
	}

	private void drawImages() {
		Platform.runLater(() -> {
			AnchorPane pane = new AnchorPane();
			getSelectedImages().stream().map(ImageData::getImage).map(img -> {
				ImageView view = new ImageView();
				view.setImage(img);
				return view;
			}).forEach(pane.getChildren()::add);
			setImageContent(pane);
		});
	}

	private ObservableList<ImageData> getSelectedImages() {
		return this.images;
	}

	private void setImageContent(AnchorPane pane) {
		content.setContent(pane);
	}

	private void showError(IOException e) {
		e.printStackTrace();
	}

	public void apply(Transformation t) {
		this.images.stream().parallel().forEach(i -> i.apply(t));
		this.drawImages();
	}

	public void apply(MultiTransformation t) {
		ObservableList<ImageData> selectedImages = getSelectedImages();
		EXECUTOR.submit(() -> {
			t.apply(selectedImages.stream().map(ImageData::getImage).collect(Collectors.toList()), result -> {
				for (int i = 0; i < selectedImages.size(); i++) {
					selectedImages.get(i).update(result.get(i));
				}
				this.drawImages();
			});
		});
		this.drawImages();
	}

	public void exportTo(FormatCatalog format) {
		if (this.getSelectedImages().isEmpty()) {
			return;
		}
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Exportar para...");
		File result = chooser.showSaveDialog(root.getScene().getWindow());
		if (result == null) {
			return;
		}
		try {
			if (!result.exists()) {
				result.createNewFile();
			}
			try (FileOutputStream out = new FileOutputStream(result)) {
				Image image = this.getSelectedImages().stream().map(ImageData::getImage).findFirst().get();
				format.to(image, out);
				out.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static class ImageData {
		private String id = UUID.randomUUID().toString();

		private String name;

		private Image image;

		public ImageData(File file) throws IOException {
			this.name = file.getName();
			this.image = new Image(file.toURI().toURL().toString(), true);
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public Image getImage() {
			return image;
		}

		public void update(Image image) {
			this.image = image;
		}

		public void apply(Transformation t) {
			this.image = t.apply(image);
		}
	}

	private static class ImageDataCell extends ListCell<ImageData> {

		@Override
		protected void updateItem(ImageData item, boolean empty) {
			super.updateItem(item, empty);
			if (empty) {
				setText(null);
			} else {
				setText(item.getName());
			}
		}
	}

	public static ImageTab create(Consumer<Tab> finishAction) {
		try {
			FXMLLoader loader = new FXMLLoader(ImageTab.class.getResource(FXML));
			finishAction.accept(loader.load());
			ImageTab controller = loader.getController();
			return controller;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
