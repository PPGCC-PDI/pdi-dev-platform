package edu.utfpr.ppgcc.ip.transformations.multi;

import java.util.List;
import java.util.function.Consumer;

import javafx.scene.image.Image;

public interface MultiTransformation {

	String getLabel();

	List<Image> apply(List<Image> image, Consumer<List<Image>> stepListener);

}
