package edu.utfpr.ppgcc.ip.transformations;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public interface ColorTransformation extends Transformation {

	Color transform(PixelReader in, int w, int h);

	default Image apply(Image image) {
		double w = image.getWidth();
		double h = image.getHeight();
		PixelReader reader = image.getPixelReader();
		WritableImage result = new WritableImage((int) w, (int) h);
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				result.getPixelWriter().setColor(i, j, this.transform(reader, i, j));
			}
		}
		return result;
	}
}
