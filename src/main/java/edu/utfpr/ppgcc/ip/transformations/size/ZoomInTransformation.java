package edu.utfpr.ppgcc.ip.transformations.size;

import edu.utfpr.ppgcc.ip.transformations.Transformation;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class ZoomInTransformation implements Transformation {

	@Override
	public String getLabel() {
		return "Zoom In";
	}

	@Override
	public Image apply(Image image) {
		int w = (int) (image.getWidth() * 2);
		int h = (int) (image.getHeight() * 2);
		WritableImage result = new WritableImage(w, h);
		PixelReader in = image.getPixelReader();
		PixelWriter out = result.getPixelWriter();
		for (int wi = 0; wi < image.getWidth(); wi++) {
			for (int hi = 0; hi < image.getHeight(); hi++) {
				int pixel = in.getArgb(wi, hi);
				int wCenter = wi * 2;
				int hCenter = hi * 2;
				out.setArgb(wCenter, hCenter, pixel);
				out.setArgb(wCenter + 1, hCenter, pixel);
				out.setArgb(wCenter, hCenter + 1, pixel);
				out.setArgb(wCenter + 1, hCenter + 1, pixel);
			}
		}
		return result;
	}

}
