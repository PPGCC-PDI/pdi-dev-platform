package edu.utfpr.ppgcc.ip.transformations.effects;

import java.util.Random;

import edu.utfpr.ppgcc.ip.transformations.Transformation;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class SaltAndPepperTransformation implements Transformation {

	private static final double CHANGE_TAX = 0.05d;

	@Override
	public String getLabel() {
		return "Sal e Pimenta";
	}

	@Override
	public Image apply(Image image) {
		int width = (int) image.getWidth();
		int height = (int) image.getHeight();
		int pixels = width * height;
		int toChange = (int) (pixels * CHANGE_TAX);
		PixelReader reader = image.getPixelReader();
		WritableImage result = new WritableImage(reader, width, height);
		PixelWriter writer = result.getPixelWriter();
		Random rand = new Random();
		for (int i = 0; i < toChange; i++) {
			int r = rand.nextInt(pixels);
			int x = r / width;
			int y = r % width;
			writer.setColor(x, y, changeColor(reader.getColor(x, y)));
		}

		return result;
	}

	private Color changeColor(Color color) {
		int med = (int) (((color.getBlue() + color.getRed() + color.getGreen()) / 3.0) * 255);
		if (med > 127) {
			return new Color(1.0, 1.0, 1.0, color.getOpacity());
		} else {
			return new Color(0.0, 0.0, 0.0, color.getOpacity());
		}
	}

}
