package edu.utfpr.ppgcc.ip.transformations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import edu.utfpr.ppgcc.ip.transformations.color.GrayScaleTransformation;
import edu.utfpr.ppgcc.ip.transformations.effects.BlurTransformation;
import edu.utfpr.ppgcc.ip.transformations.effects.MedianTransformation;
import edu.utfpr.ppgcc.ip.transformations.effects.SaltAndPepperTransformation;
import edu.utfpr.ppgcc.ip.transformations.effects.ShapenTransformation;
import edu.utfpr.ppgcc.ip.transformations.morph.PrewittBorderDetectTransformation;
import edu.utfpr.ppgcc.ip.transformations.multi.AlphaChangeTransformation;
import edu.utfpr.ppgcc.ip.transformations.multi.MultiTransformation;
import edu.utfpr.ppgcc.ip.transformations.rotation.ClockwiseTranformation;
import edu.utfpr.ppgcc.ip.transformations.rotation.CounterClockwiseTranformation;
import edu.utfpr.ppgcc.ip.transformations.rotation.HorizontalFlipTranformation;
import edu.utfpr.ppgcc.ip.transformations.rotation.VerticalFlipTranformation;
import edu.utfpr.ppgcc.ip.transformations.size.ZoomInTransformation;
import edu.utfpr.ppgcc.ip.transformations.size.ZoomOutTransformation;

public class TransformationsCatalog {
	private static final List<Transformation> color = new ArrayList<>();
	private static final List<Transformation> size = new ArrayList<>();
	private static final List<Transformation> rotation = new ArrayList<>();
	private static final List<Transformation> effect = new ArrayList<>();
	private static final List<Transformation> morph = new ArrayList<>();
	private static final List<MultiTransformation> multi = new ArrayList<>();
	static {
		color.add(new GrayScaleTransformation());
		size.add(new ZoomInTransformation());
		size.add(new ZoomOutTransformation());
		rotation.add(new HorizontalFlipTranformation());
		rotation.add(new VerticalFlipTranformation());
		rotation.add(new ClockwiseTranformation());
		rotation.add(new CounterClockwiseTranformation());
		effect.add(new MedianTransformation());
		effect.add(new ShapenTransformation());
		effect.add(new BlurTransformation());
		effect.add(new SaltAndPepperTransformation());
		morph.add(new PrewittBorderDetectTransformation());
		multi.add(new AlphaChangeTransformation());
	}

	private TransformationsCatalog() {
	}

	public static List<Transformation> color() {
		return Collections.unmodifiableList(color);
	}

	public static List<Transformation> size() {
		return Collections.unmodifiableList(size);
	}

	public static Collection<Transformation> rotation() {
		return Collections.unmodifiableList(rotation);
	}

	public static Collection<Transformation> effect() {
		return Collections.unmodifiableList(effect);
	}

	public static Collection<Transformation> morph() {
		return Collections.unmodifiableList(morph);
	}

	public static Collection<MultiTransformation> multi() {
		return Collections.unmodifiableList(multi);
	}
}
