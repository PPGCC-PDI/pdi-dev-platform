package edu.utfpr.ppgcc.ip.transformations.effects;

import java.util.function.Function;
import java.util.function.ToDoubleFunction;

import edu.utfpr.ppgcc.ip.transformations.ColorTransformation;
import edu.utfpr.ppgcc.ip.transformations.MaskIterator;
import edu.utfpr.ppgcc.ip.transformations.MaskIterator.Pixel;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class MedianTransformation implements ColorTransformation {

	@Override
	public String getLabel() {
		return "Mediana";
	}

	public Color transform(PixelReader in, int w, int h) {
		Function<ToDoubleFunction<Color>, double[]> streamGen = channelExtractor -> new MaskIterator(3, 3)
				.generateStream(in, w, h).map(Pixel::getColor).mapToDouble(channelExtractor).sorted().toArray();
		double red = median(streamGen.apply(Color::getRed));
		double green = median(streamGen.apply(Color::getGreen));
		double blue = median(streamGen.apply(Color::getBlue));
		double opacity = median(streamGen.apply(Color::getOpacity));
		return new Color(red, green, blue, opacity);
	}

	private double median(double[] apply) {
		if (apply.length == 0) {
			return 0.0;
		}
		if (apply.length % 2 != 0) {
			return apply[apply.length / 2];
		} else {
			return (apply[apply.length / 2 - 1] + apply[(apply.length) / 2]) / 2.0;
		}
	}

}
