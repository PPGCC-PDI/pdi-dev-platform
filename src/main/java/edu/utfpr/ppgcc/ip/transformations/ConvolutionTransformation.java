package edu.utfpr.ppgcc.ip.transformations;

import java.util.Iterator;
import java.util.function.ToDoubleFunction;

import edu.utfpr.ppgcc.ip.transformations.MaskIterator.Pixel;
import edu.utfpr.ppgcc.ip.transformations.effects.Mask;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public abstract class ConvolutionTransformation implements ColorTransformation {

	protected final Mask mask;

	public ConvolutionTransformation(Mask mask) {
		this.mask = mask;
	}

	@Override
	public Color transform(PixelReader in, int w, int h) {
		ToDoubleFunction<ToDoubleFunction<Color>> fn = channelExtractor -> {
			Iterator<Double> it = mask.iterator();
			return new MaskIterator(mask.getHeight(), mask.getWidth()).generateStream(in, w, h).map(Pixel::getColor)
					.mapToDouble(channelExtractor).map(v -> v * 255.0).map(v -> v * it.next())
					.reduce(0.0, this::reducer);
		};
		double red = fn.applyAsDouble(Color::getRed) / 255.0;
		double blue = fn.applyAsDouble(Color::getBlue) / 255.0;
		double green = fn.applyAsDouble(Color::getGreen) / 255.0;
		double opacity = in.getColor(w, h).getOpacity();

		return new Color(red, blue, green, opacity);
	}

	protected double reducer(double v1, double v2) {
		return v1 + v2;
	}

}
