package edu.utfpr.ppgcc.ip.transformations.rotation;

import edu.utfpr.ppgcc.ip.transformations.Transformation;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class HorizontalFlipTranformation implements Transformation {

	@Override
	public String getLabel() {
		return "Flip Horizontal";
	}

	@Override
	public Image apply(Image image) {
		int w = (int) image.getWidth();
		int h = (int) image.getHeight();
		WritableImage result = new WritableImage(w, h);
		PixelReader in = image.getPixelReader();
		PixelWriter out = result.getPixelWriter();
		for (int wi = 0; wi < image.getWidth() / 2; wi++) {
			for (int hi = 0; hi < image.getHeight(); hi++) {
				int pixel = in.getArgb(wi, hi);
				out.setArgb(wi, hi, in.getArgb(w - wi - 1, hi));
				out.setArgb(w - wi - 1, hi, pixel);
			}
		}
		return result;
	}

}
