package edu.utfpr.ppgcc.ip.transformations.rotation;

import edu.utfpr.ppgcc.ip.transformations.Transformation;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class VerticalFlipTranformation implements Transformation {

	@Override
	public String getLabel() {
		return "Flip Vertical";
	}

	@Override
	public Image apply(Image image) {
		int w = (int) image.getWidth();
		int h = (int) image.getHeight();
		WritableImage result = new WritableImage(w, h);
		PixelReader in = image.getPixelReader();
		PixelWriter out = result.getPixelWriter();
		for (int wi = 0; wi < image.getWidth(); wi++) {
			for (int hi = 0; hi < image.getHeight() / 2; hi++) {
				int pixel = in.getArgb(wi, hi);
				out.setArgb(wi, hi, in.getArgb(wi, h - hi - 1));
				out.setArgb(wi, h - hi - 1, pixel);
			}
		}
		return result;
	}

}
