package edu.utfpr.ppgcc.ip.transformations.morph;

import java.util.Iterator;
import java.util.stream.IntStream;

import edu.utfpr.ppgcc.ip.transformations.MaskIterator;
import edu.utfpr.ppgcc.ip.transformations.MaskIterator.Pixel;
import edu.utfpr.ppgcc.ip.transformations.effects.Mask;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class PrewittBorderDetectTransformation implements MorphTransformation {

	private Mask hMask = new Mask(new double[] { -1, -1, -1, 0, 0, 0, 1, 1, 1 });
	private Mask vMask = new Mask(new double[] { 1, 0, -1, 1, 0, -1, 1, 0, -1 });

	@Override
	public String getLabel() {
		return "Det. Bordas - Prewitt";
	}

	@Override
	public Color transform(PixelReader in, int x, int y) {
		int[] data = new MaskIterator(hMask.getHeight(), hMask.getWidth()).generateStream(in, x, y).sorted()
				.map(Pixel::getColor).map(Color::grayscale).mapToDouble(Color::getRed).mapToInt(d -> (int) (d * 255))
				.toArray();

		Iterator<Double> hIt = hMask.iterator();
		int corH = IntStream.of(data).map(v -> (int) (v * hIt.next())).sum();

		Iterator<Double> vIt = vMask.iterator();
		int corV = IntStream.of(data).map(v -> (int) (v * vIt.next())).sum();

		int c = (int) Math.sqrt((corH * corH) + (corV * corV));
		if (c > 255)
			c = 255;
		if (c < 0)
			c = 0;
		double eq = c / 255D;
		return new Color(eq, eq, eq, in.getColor(x, y).getOpacity());
	}

}
