package edu.utfpr.ppgcc.ip.transformations;

import javafx.scene.image.Image;

public interface Transformation {
	String getLabel();

	Image apply(Image image);
}
