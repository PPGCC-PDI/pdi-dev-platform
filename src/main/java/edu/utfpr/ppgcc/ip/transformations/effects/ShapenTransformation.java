package edu.utfpr.ppgcc.ip.transformations.effects;

import edu.utfpr.ppgcc.ip.transformations.ConvolutionTransformation;

public class ShapenTransformation extends ConvolutionTransformation {

	public ShapenTransformation() {
		super(new Mask(new double[] { -1, -1, -1, -1, 8, -1, -1, -1, -1 }));
	}

	@Override
	public String getLabel() {
		return "Shapen";
	}

}
