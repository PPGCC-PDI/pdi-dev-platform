package edu.utfpr.ppgcc.ip.transformations.multi;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class AlphaChangeTransformation implements MultiTransformation {

	private static final int STEPS = 10;

	@Override
	public String getLabel() {
		return "Alternância Alpha";
	}

	@Override
	public List<Image> apply(List<Image> image, Consumer<List<Image>> stepListener) {
		for (int i = 0; i < image.size(); i++) {
			int currIdx = i;
			int nextIdx = i + 2 >= image.size() ? 0 : i + 1;
			Image current = image.get(i);
			Image next = image.get(nextIdx);
			double stepSize = 1.0 / STEPS;
			for (int step = 0; step < STEPS; step++) {
				image.set(nextIdx, updateAlpha(next, stepSize * (double) step));
				image.set(currIdx, updateAlpha(current, 1.0 - (stepSize * (double) step)));
				Platform.runLater(() -> stepListener.accept(image));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return image;
	}

	private Image updateAlpha(Image img, double d) {
		PixelReader reader = img.getPixelReader();
		WritableImage writableImage = new WritableImage(reader, (int) img.getWidth(), (int) img.getHeight());
		PixelWriter writer = writableImage.getPixelWriter();
		IntStream.range(0, (int) img.getWidth()).forEach(x -> IntStream.range(0, (int) img.getHeight()).forEach(y -> {
			Color color = reader.getColor(x, y);
			writer.setColor(x, y, new Color(color.getRed(), color.getGreen(), color.getBlue(), d));
		}));
		return writableImage;
	}

}
