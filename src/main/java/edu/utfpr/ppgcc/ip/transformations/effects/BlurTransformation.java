package edu.utfpr.ppgcc.ip.transformations.effects;

import edu.utfpr.ppgcc.ip.transformations.ConvolutionTransformation;

public class BlurTransformation extends ConvolutionTransformation {

	public BlurTransformation() {
		super(new Mask(new double[] { -1, 2, -1, 2, 8, 2, -1, 2, -1 }));
	}

	@Override
	public String getLabel() {
		return "Blur";
	}

}
