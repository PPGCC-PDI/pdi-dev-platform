package edu.utfpr.ppgcc.ip.transformations.color;

import edu.utfpr.ppgcc.ip.transformations.ColorTransformation;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class GrayScaleTransformation implements ColorTransformation {

	@Override
	public String getLabel() {
		return "Escala de Cinza";
	}

	@Override
	public Color transform(PixelReader reader, int x, int y) {
		return reader.getColor(x, y).grayscale();
	}

}
