package edu.utfpr.ppgcc.ip.formats;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class PgmFormatAdapter implements FormatAdapter {

	private static final String MAGIC_NUMBER = "P2";
	private static final int DEFAULT_GRAYSCALE = 255;

	@Override
	public Image from(InputStream in) {

		return null;
	}

	@Override
	public void to(Image image, OutputStream out) {
		try (PrintWriter writer = new PrintWriter(out)) {
			writer.println(MAGIC_NUMBER);
			int width = (int) image.getWidth();
			int height = (int) image.getHeight();
			writer.printf("%d %d", width, height);
			writer.println();
			writer.printf("%d", DEFAULT_GRAYSCALE);
			PixelReader pixelReader = image.getPixelReader();
			for (int y = 0; y < height; y++) {
				writer.println();
				for (int x = 0; x < width; x++) {
					Color color = pixelReader.getColor(x, y);
					writer.printf("%d", (int) (color.grayscale().getRed() * 255));
					if (y != height - 1) {
						writer.write(' ');
					}
				}
			}
		}
	}

}
