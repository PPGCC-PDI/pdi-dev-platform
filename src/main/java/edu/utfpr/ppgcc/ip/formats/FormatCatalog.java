package edu.utfpr.ppgcc.ip.formats;

import java.io.InputStream;
import java.io.OutputStream;

import javafx.scene.image.Image;

public enum FormatCatalog implements FormatAdapter {
	PGM(new PgmFormatAdapter()),
	PNG(new PngFormatAdapter());
	private final FormatAdapter adapter;

	private FormatCatalog(FormatAdapter adapter) {
		this.adapter = adapter;
	}

	@Override
	public Image from(InputStream in) {
		return adapter.from(in);
	}

	@Override
	public void to(Image image, OutputStream out) {
		adapter.to(image, out);
	}

	public String getLabel() {
		return name();
	}
}
