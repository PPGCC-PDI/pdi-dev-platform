package edu.utfpr.ppgcc.ip.formats;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

public class PngFormatAdapter implements FormatAdapter {

	@Override
	public Image from(InputStream in) {
		return new Image(in);
	}

	@Override
	public void to(Image image, OutputStream out) {
		BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
		try {
			ImageIO.write(bufferedImage, "png", out);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
