package edu.utfpr.ppgcc.ip.formats;

import java.io.InputStream;
import java.io.OutputStream;

import javafx.scene.image.Image;

public interface FormatAdapter {
	Image from(InputStream in);

	void to(Image image, OutputStream out);

}
