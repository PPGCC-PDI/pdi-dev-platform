package edu.utfpr.ppgcc.ip;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import edu.utfpr.ppgcc.ip.formats.FormatCatalog;
import edu.utfpr.ppgcc.ip.transformations.Transformation;
import edu.utfpr.ppgcc.ip.transformations.TransformationsCatalog;
import edu.utfpr.ppgcc.ip.transformations.multi.MultiTransformation;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

public class MainWindow {
	public static final String FXML = "/" + MainWindow.class.getCanonicalName().replace(".", "/") + ".fxml";

	@FXML
	private BorderPane root;
	@FXML
	private ToolBar toolbar;
	@FXML
	private Button openFile;
	@FXML
	private MenuButton export;
	@FXML
	private MenuButton colorTransformations;
	@FXML
	private MenuButton sizeTransformations;
	@FXML
	private MenuButton rotationTransformations;
	@FXML
	private MenuButton morphTransformations;
	@FXML
	private MenuButton effectTransformations;
	@FXML
	private TabPane tabView;

	private BooleanProperty loading = new SimpleBooleanProperty(false);
	private BooleanProperty actionsEnabled = new SimpleBooleanProperty(false);

	private final List<ImageTab> tabsCtrls = new ArrayList<>();

	@FXML
	public void initialize() {
		this.tabView.getTabs().addListener(this::onTabsChange);
		Function<? super FormatCatalog, ? extends MenuItem> toExportMenu = i -> {
			MenuItem item = new MenuItem(i.getLabel());
			item.setOnAction(e -> Platform.runLater(() -> this.getCurrentTab().ifPresent(t -> {
				t.exportTo(i);
			})));
			return item;
		};
		Stream.of(FormatCatalog.values()).map(toExportMenu).forEach(export.getItems()::add);

		Function<? super Transformation, ? extends MenuItem> toMenu = i -> {
			MenuItem item = new MenuItem(i.getLabel());
			item.setOnAction(e -> Platform.runLater(() -> this.getCurrentTab().ifPresent(t -> t.apply(i))));
			return item;
		};
		TransformationsCatalog.color().stream().map(toMenu).forEach(colorTransformations.getItems()::add);
		TransformationsCatalog.size().stream().map(toMenu).forEach(sizeTransformations.getItems()::add);
		TransformationsCatalog.rotation().stream().map(toMenu).forEach(rotationTransformations.getItems()::add);
		TransformationsCatalog.morph().stream().map(toMenu).forEach(morphTransformations.getItems()::add);
		TransformationsCatalog.effect().stream().map(toMenu).forEach(effectTransformations.getItems()::add);
		Function<? super MultiTransformation, ? extends MenuItem> toMultiMenu = i -> {
			MenuItem item = new MenuItem(i.getLabel());
			item.setOnAction(e -> Platform.runLater(() -> this.getCurrentTab().ifPresent(t -> t.apply(i))));
			return item;
		};
		TransformationsCatalog.multi().stream().map(toMultiMenu).forEach(effectTransformations.getItems()::add);
	}

	@FXML
	public void showFileInput() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Selecione uma imagem");
		List<File> list = fileChooser.showOpenMultipleDialog(root.getScene().getWindow());
		if (list != null && !list.isEmpty()) {
			ImageTab t = ImageTab.create(tabView.getTabs()::add);
			t.add(list);
			tabsCtrls.add(t);
		}
	}

	public Optional<ImageTab> getCurrentTab() {
		int idx = this.tabView.getSelectionModel().getSelectedIndex();
		if (idx >= 0) {
			return Optional.of(tabsCtrls.get(idx));
		} else {
			return Optional.empty();
		}
	}

	private void onTabsChange(ListChangeListener.Change<? extends Tab> change) {
		if (change.next()) {
			if (change.wasRemoved())
				change.getRemoved().forEach(t -> this.tabsCtrls.removeIf(ctrl -> ctrl.isControllerOf(t)));
			if (change.wasAdded())
				this.tabView.getSelectionModel().selectLast();
		}
	}

	public BooleanProperty loadingProperty() {
		return loading;
	}

	public BooleanProperty actionsEnabledProperty() {
		return actionsEnabled;
	}
}
